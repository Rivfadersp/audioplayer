#include <styles.h>

QString Styles::play_btn_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/play.png)"
            "}"
            "QToolButton:hover { "
            "image: url(:/Resources/play_hovered.png)"
            "}";
}

QString Styles::add_btn_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/add.png)"
            "}"
            "QToolButton:hover { "
            "image: url(:/Resources/add_hovered.png)"
            "}";
}

QString Styles::previous_btn_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/prev.png)"
            "}"
            "QToolButton:hover { "
            "image: url(:/Resources/prev_hovered.png)"
            "}";
}

QString Styles::pause_btn_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/pause.png)"
            "}"
            "QToolButton:hover { "
            "image: url(:/Resources/pause_hovered.png)"
            "}";
}

QString Styles::next_btn_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/next.png)"
            "}"
            "QToolButton:hover { "
            "image: url(:/Resources/next_hovered.png)"
            "}";
}

QString Styles::delete_btn_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/delete.png)"
            "}"
            "QToolButton:hover { "
            "image: url(:/Resources/delete_hovered.png)"
            "}";
}

QString Styles::play_btn_clicked_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/play_hovered.png)}";
}

QString Styles::pause_btn_clicked_style()
{
    return "QToolButton { "
            "border: none;"
            "image: url(:/Resources/pause_hovered.png)}";
}
