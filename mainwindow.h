#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QFileDialog>
#include <QDir>
#include <QPainter>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_add_clicked();
    void seek(int seconds);
    void positionChanged(qint64 progress);
    void durationChanged(qint64 duration);
    void played();
    void paused();
    void del();

private:
    Ui::MainWindow *ui;
    QStandardItemModel  *m_playListModel = nullptr;
    QMediaPlayer *m_player = nullptr;
    QMediaPlaylist *m_playlist = nullptr;
};

#endif // MAINWINDOW_H
