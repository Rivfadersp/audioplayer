#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "styles.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //this->setAttribute(Qt::WA_TranslucentBackground);
    ui->playlistView->setStyleSheet("background-color: #D8D8D8");

    ui->btn_play->setStyleSheet(Styles::play_btn_style());
    ui->btn_add->setStyleSheet(Styles::add_btn_style());
    ui->btn_previous->setStyleSheet(Styles::previous_btn_style());
    ui->btn_pause->setStyleSheet(Styles::pause_btn_style());
    ui->btn_next->setStyleSheet(Styles::next_btn_style());
    ui->btn_next->setStyleSheet(Styles::next_btn_style());
    ui->btn_delete->setStyleSheet(Styles::delete_btn_style());
    ui->volume->setStyleSheet("border: none");
    this->setFixedSize(530,300);

    m_playListModel = new QStandardItemModel(this);
    ui->playlistView->setModel(m_playListModel);
    m_playListModel->setHorizontalHeaderLabels(QStringList()  << tr("Audio Track")
                                                                << tr("File Path"));
    ui->playlistView->hideColumn(1);
    ui->playlistView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->playlistView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->playlistView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->playlistView->horizontalHeader()->setStretchLastSection(true);

    m_player = new QMediaPlayer(this);
    m_playlist = new QMediaPlaylist(m_player);
    m_player->setPlaylist(m_playlist);
    m_player->setVolume(70);
    m_playlist->setPlaybackMode(QMediaPlaylist::Loop);

    connect(ui->btn_previous, &QToolButton::clicked, m_playlist, &QMediaPlaylist::previous);
    connect(ui->btn_next, &QToolButton::clicked, m_playlist, &QMediaPlaylist::next);
    connect(ui->btn_play, &QToolButton::clicked, this, &MainWindow::played);
    connect(ui->btn_pause, &QToolButton::clicked, this, &MainWindow::paused);
    connect(ui->btn_delete, &QToolButton::clicked, this, &MainWindow::del);
    connect(ui->volumeSlider, SIGNAL(valueChanged(int)), m_player, SLOT(setVolume(int)));
    connect(ui->positionSlider, &QSlider::sliderMoved, this, &MainWindow::seek);

    connect(m_player, &QMediaPlayer::positionChanged, this, &MainWindow::positionChanged);
    connect(m_player, &QMediaPlayer::durationChanged, this, &MainWindow::durationChanged);

    connect(ui->playlistView, &QTableView::doubleClicked, [this](const QModelIndex &index){
        m_playlist->setCurrentIndex(index.row());
    });

    connect(m_playlist, &QMediaPlaylist::currentIndexChanged, [this](int index){
       ui->currentTrack->setText(m_playListModel->data(m_playListModel->index(index, 0)).toString());
       //ui->currentTrack->setText("Kek");
    });
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_playListModel;
    delete m_playlist;
    delete m_player;
}

void MainWindow::on_btn_add_clicked()
{
    QStringList files = QFileDialog::getOpenFileNames(this,
                                                      tr("Open files"),
                                                      QString(),
                                                      tr("Audio Files (*.mp3 *.wav)"));
    foreach (QString filePath, files) {
        QList<QStandardItem *> items;
        items.append(new QStandardItem(QDir(filePath).dirName()));
        items.append(new QStandardItem(filePath));
        m_playListModel->appendRow(items);
        m_playlist->addMedia(QUrl::fromLocalFile(filePath));
    }
}

void MainWindow::del()
{
    int currentIndex = m_playlist->currentIndex();
    m_playListModel->removeRow(currentIndex);
    m_playlist->removeMedia(currentIndex);
    m_playlist->setCurrentIndex(currentIndex-1);
    m_playlist->next();
    m_player->play();
}


void MainWindow::seek(int seconds)
{
    m_player->setPosition(seconds * 1000);
}

void MainWindow::positionChanged(qint64 progress)
{
    ui->positionSlider->setValue(progress / 1000);
}

void MainWindow::durationChanged(qint64 duration)
{
    ui->positionSlider->setMaximum(duration / 1000);
}

void MainWindow::played()
{
    m_player->play();
    ui->btn_play->setStyleSheet(Styles::play_btn_clicked_style());
    ui->btn_pause->setStyleSheet(Styles::pause_btn_style());
}

void MainWindow::paused()
{
    m_player->pause();
    ui->btn_pause->setStyleSheet(Styles::pause_btn_clicked_style());
    ui->btn_play->setStyleSheet(Styles::play_btn_style());
}
