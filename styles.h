#ifndef STYLES_H
#define STYLES_H

#endif // STYLES_H

#include <QString>

class Styles
{
public:
    static QString play_btn_style();
    static QString add_btn_style();
    static QString previous_btn_style();
    static QString pause_btn_style();
    static QString next_btn_style();
    static QString delete_btn_style();
    static QString play_btn_clicked_style();
    static QString pause_btn_clicked_style();
};
